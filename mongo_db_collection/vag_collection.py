from framwork_from_scratch.utilities.data_base.base_db_mongo import BaseMongo


class VagMongo(BaseMongo):
    def __init__(self):
        super(VagMongo, self).__init__(db_name='cars', collection_name='vag')