import configparser
import pathlib

abs_path = pathlib.Path(__file__).parent.parent.resolve() #шлях до рут проекта
config = configparser.RawConfigParser()
config.read(f'{abs_path}/configurations/configuration.ini')


class ReadConfig:
    @staticmethod
    def get_base_url():
        return config.get('app_info', 'base_url')

    @staticmethod
    def get_user_name():
        return config.get('user_data', 'user_name')

    @staticmethod
    def get_user_password():
        return config.get('user_data', 'password')

    @staticmethod
    def get_browser_id():
        return config.get('browser_data', 'browser_id')
