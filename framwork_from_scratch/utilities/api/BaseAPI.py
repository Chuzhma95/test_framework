from framwork_from_scratch.utilities.configuration import config
import requests
import json


class BaseAPI:
    def __init__(self):
        self.__base_url_api = config.base_api_url
        self.__user = 'admin'
        self.__headers = {'x-api-key': config.x_api_key}
        self.__request = requests

    def get(self, url, headers=None):
        if headers is None:
            headers = self.__headers
        response = self.__request.get(f'{self.__base_url_api}{url}', headers=headers)
        return response

    def post(self, url, body={'test': 'test123'}, headers=None):
        if headers is None:
            headers = self.__headers
        json_obj = json.dumps(body)
        response = self.__request.post(f'{self.__base_url_api}{url}', headers=headers, json=json_obj)
        return response

