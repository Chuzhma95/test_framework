import json
import pathlib


class Configuration:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


absolute = pathlib.Path(__file__).parent.parent.resolve() #шлях до рут проекта

with open(f'{absolute}/configurations/configuration.json') as f:
    data = f.read()
    json_to_dict = json.loads(data)

config = Configuration(**json_to_dict)
