import psycopg2

from framwork_from_scratch.utilities.configuration import config


class BaseRepo:
    def __init__(self):
        self._connection = psycopg2.connect(user=config.user_db,
                                            password=config.password_db,
                                            host=config.host_db,
                                            port=config.port_db,
                                            database=config.database
                                            )
        self._connection.set_session(autocommit=True)
        self._cursor = self._connection.cursor()
        self.table_name = None

    def get_all(self):
        self._cursor.execute(f"select * from {self.table_name};")
        return self._cursor.fetchall()

    def __del__(self):
        if self._connection:
            if self._cursor:
                self._cursor.close()
            self._connection.close()
