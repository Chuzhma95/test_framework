import pymongo

from framwork_from_scratch.utilities.configuration import config


class BaseMongo:
    def __init__(self, db_name, collection_name):
        self.client = pymongo.MongoClient(config.mongo_connection_string)
        self.db = self.client[db_name]
        self.collection = self.db[collection_name]

    def insert_one(self, value):
        self.collection.insert_one(value)

    def insert_many(self, *args):
        self.collection.insert_many(args)

    def find_name(self, value):
        cursor = self.collection.find_one(value)
        return cursor

    def find_and_sort(self, find_object, sort_key):
        result = self.collection.find(find_object).sort(sort_key)
        return result

    def delete_for_model(self, value):
        delete = self.collection.delete_one(value)
        return delete