import json
from http import HTTPStatus
from framwork_from_scratch.api_collection.zippo_api import ZippoAPI
from framwork_from_scratch.data_api_classes.zippo import Zippo


def test_get_zippo():
    response = ZippoAPI().get_zippo_by_id(33162)
    assert response.status_code == HTTPStatus.OK


def test_get_zippo_negative():
    response = ZippoAPI().get_zippo_by_id('sdfsdfs')
    assert response.status_code == HTTPStatus.NOT_FOUND


def test_response_body(create_zippo):
    expected_zippo = create_zippo
    response = ZippoAPI().get_zippo_by_id(33162)
    from_json = json.loads(response.text)
    actual_zippo = Zippo.create_from_json(**from_json)
    assert actual_zippo == expected_zippo


def test_create_zippo():
    response = ZippoAPI().create_zippo(zip_id=33162, body={'kind': 'hippopotamus'})
    assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED

