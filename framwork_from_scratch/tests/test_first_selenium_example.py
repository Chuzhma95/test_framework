# import time
# import random
#
# import pytest
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.wait import WebDriverWait
# from framwork_from_scratch.page_objects.products import ProductPage
# from framwork_from_scratch.utilities.waits import wait_until
#
#
# # LOGIN!!!!
# @pytest.mark.smoke
# def test_success_user_login(open_login_page):
#     login_page = open_login_page
#     dashboard_page = login_page.login('admin@yourstore.com', 'admin')
#     assert dashboard_page.is_logout_visible() is True, 'User was not logged-in'
#
#
# @pytest.mark.regression
# def test_check_wrong_password1(open_login_page):
#     login_page = open_login_page
#     login_page.login('admin@yourstore.com', '123')
#     login_page.wrong_message_is_visible()
#
#
# @pytest.mark.regression
# def test_check_wrong_password2(open_login_page):
#     login_page = open_login_page
#     login_page.login('admin@yourstore.com', '')
#     login_page.wrong_message_is_visible()
#
#
# @pytest.mark.regression
# def test_check_empty_fields(open_login_page):
#     login_page = open_login_page
#     login_page.login('', '')
#     login_page.wrong_message_is_visible()
#
#
# @pytest.mark.regression
# def test_check_wrong_email(open_login_page):
#     login_page = open_login_page
#     login_page.login('', 'admin')
#     login_page.wrong_message_is_visible()
#
#
# # DASBOARD !!!!!
# @pytest.mark.smoke
# def test_transition_product_page(dashboard_page):
#     dashboard_page.click_catalog()
#     dashboard_page.is_product_visible()
#     product_page = dashboard_page.click_product()
#     assert product_page.is_add_new_visible() is True, 'Page not found'
#
#
# @pytest.mark.regression
# def test_transition_order_page(dashboard_page):
#     dashboard_page.click_sale()
#     dashboard_page.is_orders_visible()
#     order_page = dashboard_page.click_orders()
#     assert order_page.is_export_visible() is True, 'Page not found'
#
#
# @pytest.mark.regression
# def test_transition_system_information_page(dashboard_page):
#     dashboard_page.click_system()
#     dashboard_page.is_system_information_visible()
#     system_page = dashboard_page.click_system_information()
#     assert system_page.is_nopCommerce_visible() is True, 'Page not found'
#
#
# @pytest.mark.regression
# def test_visible_static(dashboard_page):
#     dashboard_page.click_static_plus()
#     assert dashboard_page.is_small_box_stat_info() is True, 'Block info not found'
#     assert dashboard_page.is_small_box_stat_pending() is True, 'Block pending not found'
#     assert dashboard_page.is_small_box_stat_registered() is True, 'Block registrated not found'
#     assert dashboard_page.is_small_box_stat_products() is True, 'Block products not found'
#
#
# @pytest.mark.regression
# def test_relocate_order_page(dashboard_page):
#     vew_order_page = dashboard_page.vew_button_order()
#     assert vew_order_page.is_export_visible() is True, 'Page not found'
#
#
# # PRODUCTS!!!!!
#
# @pytest.mark.smoke
# def test_product_name_specific_search(open_product_page):
#     product_page = open_product_page
#     product_page.set_product_name('Build your own computer')
#     product_page.click_search()
#     assert product_page.is_product_name_search_res_visible() is True, 'Products are not'
#     assert product_page.is_items_1_visible() is True, 'Product more or less 1'
#
#
# # def test_product_name1_search(open_product_page): МОЯ ЧЕРНЕТКА
# #     product_page = open_product_page
# #     elements = product_page.wait_until_elements_located((By.CSS_SELECTOR, 'tbody tr td:nth-child(3)'))
# #     elem_names = [elem.accessible_name for elem in elements]
# #     r_element = random.choice(elem_names)
# #     product_page.set_product_name(r_element)
# #     product_page.click_search()
# #     assert product_page.is_items_1_visible() is True, 'Product more or less 1'
#
# @pytest.mark.regression
# def test_product_name_search_amount(open_product_page):  # приклад того як правильно витягувати з ліста дані
#     product_page = open_product_page
#     elem_names = product_page.get_product_names()
#     r_element = random.choice(elem_names)
#     product_page.set_product_name(r_element)
#     product_page.click_search()
#     assert product_page.is_items_1_visible() is True, 'Product more or less 1'
#
#
# @pytest.mark.regression
# def test_product_name_search_similar(open_product_page):
#     product_page = open_product_page
#     elem_names = product_page.get_product_names()
#     r_element = random.choice(elem_names)
#     product_page.set_product_name(r_element)
#     product_page.click_search()
#     wait_until(lambda: len(product_page.get_product_names()) == 1, 'Product name is not displayed')
#     filtered_elem_names = product_page.get_product_names()
#     assert r_element == filtered_elem_names[0]
#
#
# @pytest.mark.regression
# def test_filter_type_product(open_product_page):
#     product_page = open_product_page
#     product_page.filter_product_type()
#     product_page.click_search()
#     assert product_page.is_items_6_visible() is True, 'Not the correct amount'
#
#
# @pytest.mark.regression
# def test_filter_type_and_brand_product(open_product_page):
#     product_page = open_product_page
#     product_page.filter_product_type()
#     product_page.filter_product_brand()
#     product_page.click_search()
#     assert product_page.is_items_1_visible() is True, 'Not the correct amount'
#
#
# # SALE!!!!
# @pytest.mark.smoke
# def test_select_country(open_order_page):
#     order_page = open_order_page
#     order_page.select_country()
#     order_page.click_search()
#     assert order_page.empty_country_result_visible() is True, 'Country have products'
#
#
# @pytest.mark.regression
# def test_order_customer(open_order_page):
#     order_page = open_order_page
#     order_page.click_search_status()
#     order_page.click_status_paid()
#     order_page.click_search()
#     assert order_page.is_items_3_visible() is True, 'Customer not found'
#
#
# @pytest.mark.regression
# def test_order_costumer_email_amount(open_order_page):  # приклад того як правильно витягувати з ліста дані і перезаписувати
#     order_page = open_order_page
#     elem_email = order_page.get_customer_email()
#     r_email = random.choice(elem_email)
#     order_page.set_email_address(r_email)
#     order_page.click_search()
#     assert order_page.is_items_1_visible() is True, 'Email not found'
#
#
# @pytest.mark.regression
# def test_order_costumer_email_similar(open_order_page):  # не працює
#     order_page = open_order_page
#     elem_email = order_page.get_customer_email()
#     r_email = random.choice(elem_email)
#     order_page.set_email_address(r_email)
#     order_page.click_search()
#     wait_until(lambda: len(order_page.get_customer_email()) == 1, 'Email not found')
#     filtered_elem_name = order_page.get_customer_email()
#     assert r_email == filtered_elem_name[0]
#
#
# @pytest.mark.regression
# def test_check_shipping_statuses(open_order_page):
#     order_page = open_order_page
#     order_page.click_shipping_statuses()
#     order_page.click_delivered_status()
#     order_page.click_search()
#     assert order_page.is_items_1_visible() is True, 'Product not found'
#
#
# # SYSTEM
# @pytest.mark.smoke
# def test_check_field_value_version(open_system_information_page):
#     system_info = open_system_information_page
#     assert system_info.visible_nopCommerce_version_field() is True, 'Field not found'
#     assert system_info.visible_nopCommerce_version_value() is True, 'Not the correct version'
#
#
# @pytest.mark.smoke
# def test_check_field_value_operation(open_system_information_page):
#     system_info = open_system_information_page
#     assert system_info.visible_operation_system_field() is True, 'Field not found'
#     assert system_info.visible_nopCommerce_version_value() is True, 'Not the correct operation'
#
#
# @pytest.mark.smoke
# def test_check_field_value_asp(open_system_information_page):
#     system_info = open_system_information_page
#     assert system_info.visible_asp_field() is True, 'Field not found'
#     assert system_info.visible_asp_value() is True, 'Not the correct asp'
#
#
# @pytest.mark.smoke
# def test_check_field_value_level(open_system_information_page):
#     system_info = open_system_information_page
#     assert system_info.visible_trust_level_field() is True, 'Field not found'
#     assert system_info.visible_trust_level_value() is True, 'Not the correct level'
#
#
# @pytest.mark.smoke
# def test_check_field_value_time(open_system_information_page):
#     system_info = open_system_information_page
#     assert system_info.visible_time_zone_field() is True, 'Field not found'
#     assert system_info.visible_time_zone_value() is True, 'Not the correct time zone'
#
