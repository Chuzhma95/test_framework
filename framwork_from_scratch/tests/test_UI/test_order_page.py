import time
import random
import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from framwork_from_scratch.page_objects.products import ProductPage
from framwork_from_scratch.utilities.waits import wait_until


@pytest.mark.parametrize('country', ['Ukraine', 'Poland'])
@pytest.mark.smoke
def test_select_country(open_order_page, country):
    order_page = open_order_page
    order_page.select_country(country)
    order_page.click_search()
    assert order_page.empty_country_result_visible() is True, 'Country have products'


@pytest.mark.regression
def test_order_customer(open_order_page):
    order_page = open_order_page
    if not order_page.is_arrow_up_visible():
        order_page.click_arrow_down()
        order_page.is_arrow_up_visible()
    order_page.click_search_status()
    order_page.click_status_paid()
    order_page.click_search()
    assert order_page.is_items_3_visible() is True, 'Customer not found'


@pytest.mark.regression
def test_order_costumer_email_amount(open_order_page):  # приклад того як правильно витягувати з ліста дані і перезаписувати
    order_page = open_order_page
    elem_email = order_page.get_customer_emails()
    r_email = random.choice(elem_email)
    order_page.set_email_address(r_email)
    order_page.click_search()
    assert order_page.is_items_1_visible() is True, 'Email not found'


@pytest.mark.regression
def test_order_costumer_email_similar(open_order_page):
    order_page = open_order_page
    elem_email = order_page.get_customer_emails()
    r_email = random.choice(elem_email)
    order_page.set_email_address(r_email)
    order_page.click_search()
    wait_until(lambda: len(order_page.get_customer_emails()) == 1, 'Email not found')
    filtered_elem_name = order_page.get_customer_emails()
    assert r_email == filtered_elem_name[0]


@pytest.mark.regression
def test_check_shipping_statuses(open_order_page):
    order_page = open_order_page
    order_page.click_shipping_statuses()
    order_page.click_delivered_status()
    order_page.click_search()
    assert order_page.is_items_1_visible() is True, 'Product not found'

