import pytest
from framwork_from_scratch.utilities.configuration import config


@pytest.mark.smoke
def test_success_user_login(open_login_page):
    login_page = open_login_page
    dashboard_page = login_page.login(config.user_name, config.user_password)
    assert dashboard_page.is_logout_visible() is True, 'User was not logged-in'


@pytest.mark.parametrize('login, password', [('admin@yourstore.com', 123), ('admin@yourstore.com', ''), ('admin123@yourstore.com', 'admin')])
def test_check_wrong_password(open_login_page, login, password):
    login_page = open_login_page
    login_page.login(login, password)
    assert login_page.wrong_message_is_visible() is True

# @pytest.mark.regression #my DRAFT
# def test_check_wrong_password1(open_login_page):
#     login_page = open_login_page
#     login_page.login('admin@yourstore.com', '123')
#     login_page.wrong_message_is_visible()


@pytest.mark.parametrize('login, password', [('', 123), ('', '')])
def test_check_empty_fields(open_login_page, login, password):
    login_page = open_login_page
    login_page.login(login, password)
    assert login_page.email_error_is_visible() is True


# @pytest.mark.regression #my DRAFT
# def test_check_wrong_email(open_login_page):
#     login_page = open_login_page
#     login_page.login('', 'admin')
#     login_page.wrong_message_is_visible()