import time
import random
import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from framwork_from_scratch.page_objects.products import ProductPage
from framwork_from_scratch.utilities.waits import wait_until


@pytest.mark.smoke
def test_check_field_value_version(open_system_information_page):
    system_info = open_system_information_page
    assert system_info.visible_nopCommerce_version_field() is True, 'Field not found'
    assert system_info.visible_nopCommerce_version_value() is True, 'Not the correct version'


@pytest.mark.smoke
def test_check_field_value_operation(open_system_information_page):
    system_info = open_system_information_page
    assert system_info.visible_operation_system_field() is True, 'Field not found'
    assert system_info.visible_nopCommerce_version_value() is True, 'Not the correct operation'


@pytest.mark.smoke
def test_check_field_value_asp(open_system_information_page):
    system_info = open_system_information_page
    assert system_info.visible_asp_field() is True, 'Field not found'
    assert system_info.visible_asp_value() is True, 'Not the correct asp'


@pytest.mark.smoke
def test_check_field_value_level(open_system_information_page):
    system_info = open_system_information_page
    assert system_info.visible_trust_level_field() is True, 'Field not found'
    assert system_info.visible_trust_level_value() is True, 'Not the correct level'


@pytest.mark.smoke
def test_check_field_value_time(open_system_information_page):
    system_info = open_system_information_page
    assert system_info.visible_time_zone_field() is True, 'Field not found'
    assert system_info.visible_time_zone_value() is True, 'Not the correct time zone'
