import time
import random
import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from framwork_from_scratch.page_objects.products import ProductPage
from framwork_from_scratch.utilities.waits import wait_until


@pytest.mark.smoke
def test_product_name_specific_search(open_product_page):
    product_page = open_product_page
    product_page.set_product_name('Build your own computer')
    product_page.click_search()
    assert product_page.is_product_name_search_res_visible() is True, 'Products are not'
    assert product_page.is_items_1_visible() is True, 'Product more or less 1'


# def test_product_name1_search(open_product_page): МОЯ ЧЕРНЕТКА
#     product_page = open_product_page
#     elements = product_page.wait_until_elements_located((By.CSS_SELECTOR, 'tbody tr td:nth-child(3)'))
#     elem_names = [elem.accessible_name for elem in elements]
#     r_element = random.choice(elem_names)
#     product_page.set_product_name(r_element)
#     product_page.click_search()
#     assert product_page.is_items_1_visible() is True, 'Product more or less 1'

@pytest.mark.regression
def test_product_name_search_amount(open_product_page):  # приклад того як правильно витягувати з ліста дані
    product_page = open_product_page
    elem_names = product_page.get_product_names()
    r_element = random.choice(elem_names)
    product_page.set_product_name(r_element)
    product_page.click_search()
    assert product_page.is_items_1_visible() is True, 'Product more or less 1'


@pytest.mark.regression
def test_product_name_search_similar(open_product_page):
    product_page = open_product_page
    elem_names = product_page.get_product_names()
    r_element = random.choice(elem_names)
    product_page.set_product_name(r_element)
    product_page.click_search()
    wait_until(lambda: len(product_page.get_product_names()) == 1, 'Product name is not displayed')
    filtered_elem_names = product_page.get_product_names()
    assert r_element == filtered_elem_names[0]


@pytest.mark.parametrize('product_type', ['Computers >> Notebooks'])
@pytest.mark.regression
def test_filter_type_product(open_product_page, product_type):
    product_page = open_product_page
    product_page.filter_product_type(product_type)
    product_page.click_search()
    assert product_page.is_items_6_visible() is True, 'Not the correct amount'


@pytest.mark.parametrize('product_brand', ['Apple'])
@pytest.mark.regression
def test_filter_brand_product(open_product_page, product_brand):
    product_page = open_product_page
    product_page.filter_product_brand(product_brand)
    product_page.click_search()
    assert product_page.is_items_2_visible() is True, 'Not the correct amount'
