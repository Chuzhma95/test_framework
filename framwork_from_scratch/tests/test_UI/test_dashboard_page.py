import time
import random
import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from framwork_from_scratch.page_objects.products import ProductPage
from framwork_from_scratch.utilities.waits import wait_until


@pytest.mark.smoke
def test_transition_product_page(dashboard_page):
    dashboard_page.click_catalog()
    dashboard_page.is_product_visible()
    product_page = dashboard_page.click_product()
    assert product_page.is_add_new_visible() is True, 'Page not found'


@pytest.mark.regression
def test_transition_order_page(dashboard_page):
    dashboard_page.click_sale()
    dashboard_page.is_orders_visible()
    order_page = dashboard_page.click_orders()
    assert order_page.is_export_visible() is True, 'Page not found'


@pytest.mark.regression
def test_transition_system_information_page(dashboard_page):
    dashboard_page.click_system()
    dashboard_page.is_system_information_visible()
    system_page = dashboard_page.click_system_information()
    assert system_page.is_nopCommerce_visible() is True, 'Page not found'


@pytest.mark.regression
def test_visible_static(dashboard_page):
    dashboard_page.click_static_plus()
    assert dashboard_page.is_small_box_stat_info() is True, 'Block info not found'
    assert dashboard_page.is_small_box_stat_pending() is True, 'Block pending not found'
    assert dashboard_page.is_small_box_stat_registered() is True, 'Block registrated not found'
    assert dashboard_page.is_small_box_stat_products() is True, 'Block products not found'


@pytest.mark.regression
def test_relocate_order_page(dashboard_page):
    vew_order_page = dashboard_page.vew_button_order()
    assert vew_order_page.is_export_visible() is True, 'Page not found'