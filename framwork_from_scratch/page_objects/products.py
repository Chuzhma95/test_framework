from framwork_from_scratch.utilities.web_ui.base_page import BasePage
from selenium.webdriver.common.by import By
from typing import List
import allure


class ProductPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    __add_new = (By.XPATH, '//a[@class="btn btn-primary"]')
    __product_name_input = (By.XPATH, '//*[@id="SearchProductName"]')
    __search_button = (By.XPATH, '//*[@id="search-products"]')
    __items_1 = (By.XPATH, '//*[text() = "1-1 of 1 items"]')
    __items_2 = (By.XPATH, '//*[text() = "1-2 of 2 items"]')
    __items_6 = (By.XPATH, '//*[text()="1-6 of 6 items"]')
    __product_name_search_res = (By.XPATH, '//td[text() = "Build your own computer"]')
    __product_names = (By.XPATH, '//tbody/tr/td[3]')
    __select_type_name = (By.XPATH, '//*[@id="SearchCategoryId"]')
    __select_brand_name = (By.XPATH, '//*[@id="SearchManufacturerId"]')

    @allure.step
    def is_add_new_visible(self) -> bool:
        """
        for check that it is dashboard page
        :return: button "Add new"
        """
        return self._is_visible(self.__add_new)

    @allure.step
    def set_product_name(self, product_name) -> 'ProductPage':
        """
        for enter product name in the field
        :param product_name: product name
        :return:
        """
        self._send_keys(self.__product_name_input, product_name)
        return self

    @allure.step
    def click_search(self) -> None:
        """
        for search product data
        :return: -
        """
        self._click(self.__search_button)

    @allure.step
    def is_items_1_visible(self) -> bool:
        """
        that the page displays only one product that we were looking for
        :return: text "1-1 of 1 items"
        """
        return self._is_visible(self.__items_1)

    @allure.step
    def is_items_2_visible(self) -> bool:
        """
        that the page displays only one product that we were looking for
        :return: text "1-2 of 2 items"
        """
        return self._is_visible(self.__items_2)

    @allure.step
    def is_items_6_visible(self) -> bool:
        """
        that the page displays only six products that we were looking for
        :return: text "1-6 of 6 items"
        """
        return self._is_visible(self.__items_6)

    @allure.step
    def is_product_name_search_res_visible(self) -> bool:
        """
        to check the display of a specific product "Build your own computer"
        :return: the name of the searched element "Build your own computer"
        """
        return self._is_visible(self.__product_name_search_res)

    @allure.step
    def get_product_names(self) -> List[str]:
        """
        allows you to get the names of all products that are on the page
        :return: products name
        """
        elements = self._get_elements(self.__product_names)
        return [elem.accessible_name for elem in elements]

    @allure.step
    def filter_product_type(self, product_type: str) -> None:
        """
        filtering by type "Computers >> Notebooks"
        :return: selected list with type "Computers >> Notebooks"
        """
        self._select_element(self.__select_type_name, product_type)

    @allure.step
    def filter_product_brand(self, product_brand: str) -> None:
        """
        filtering by type "Computers >> Notebooks"
        :return: selected list with brand "Apple"
        """
        self._select_element(self.__select_brand_name, product_brand)