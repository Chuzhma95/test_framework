from framwork_from_scratch.utilities.web_ui.base_page import BasePage
from selenium.webdriver.common.by import By
from typing import List
import allure


class OrderPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    __export_button = (By.XPATH, '//*[@class="fas fa-download"]')
    __title_order = (By.XPATH, '//*[@class="float-left"]')
    __email_address = (By.XPATH, '//*[@id="BillingEmail"]')
    __button_search = (By.XPATH, '//*[@id="search-orders"]')
    __items_1 = (By.XPATH, '//*[text()="1-1 of 1 items"]')
    __items_3 = (By.XPATH, '//*[text()="1-3 of 3 items"]')
    __items_2 = (By.XPATH, '//*[text()="1-2 of 2 items"]')
    __select_status = (By.XPATH, '//input[@aria-labelledby="PaymentStatusIds_label"]')
    __status_paid = (By.XPATH, '//*[@id="PaymentStatusIds_listbox"]/li[4]')
    __billing_country_select = (By.XPATH, '//*[@id="BillingCountryId"]')
    __empty_records = (By.XPATH, '//*[@id="orders-grid_info"]')
    __customer_emails = (By.XPATH, '//tbody/tr/td[6]/a[contains(text(), "@")]')
    __shipping_statuses = (By.XPATH, '//*[@aria-describedby="ShippingStatusIds_taglist"]')
    __delivered_status = (By.XPATH, '//*[@id="ShippingStatusIds_listbox"]/li[@data-offset-index="5"]')
    __arrow_up = (By.XPATH, '//*[@class="far fa-angle-up"]')
    __arrow_down = (By.XPATH, '//*[@class="far fa-angle-down"]')

    @allure.step
    def is_export_visible(self) -> bool:
        """
        there is a check that the page "Order" has been opened
        :return: button "Export"
        """
        return self._is_visible(self.__export_button)

    @allure.step
    def is_title_order_visible(self) -> bool:
        """
        there is a check that the page "Order" has been opened
        :return: text Order
        """
        return self._is_visible(self.__title_order)

    @allure.step
    def set_email_address(self, customer_email) -> "OrderPage":
        return self._send_keys(self.__email_address, customer_email)

    @allure.step
    def click_search(self) -> None:
        self._click(self.__button_search)

    @allure.step
    def is_items_1_visible(self) -> bool:
        """
        that the page displays only one order that we were looking for
        :return: text "1-1 of 1 items"
        """
        return self._is_visible(self.__items_1)

    @allure.step
    def is_items_2_visible(self) -> bool:
        """
        that the page displays only one order that we were looking for
        :return: text "1-1 of 1 items"
        """
        return self._is_visible(self.__items_2)

    @allure.step
    def is_items_3_visible(self) -> bool:
        """
        that the page displays only 3 order that we were looking for
        :return: text "1-3 of 3 items"
        """
        return self._is_visible(self.__items_3)

    @allure.step
    def click_search_status(self) -> None:
        self._click(self.__select_status)

    @allure.step
    def click_status_paid(self) -> None:
        self._click(self.__status_paid)

    @allure.step
    def select_country(self, country: str) -> None:
        """
        select Ukraine country in list
        :return: text "Ukraine" in the field "country"
        """
        self._select_element(self.__billing_country_select, country)

    @allure.step
    def empty_country_result_visible(self) -> bool:
        """
        check that there are no orders when selecting a country in which there are no orders
        :return: text "empty records"
        """
        return self._is_visible(self.__empty_records)

    @allure.step
    def get_customer_emails(self) -> List[str]:
        """
        select the entire list of emails that are on the page
        :return:list customer with emails
        """
        elements_email = self._get_elements(self.__customer_emails)
        return [elem.accessible_name for elem in elements_email]

    @allure.step
    def click_shipping_statuses(self) -> None:
        self._click(self.__shipping_statuses)

    @allure.step
    def click_delivered_status(self) -> None:
        self._click(self.__delivered_status)

    @allure.step
    def is_arrow_up_visible(self) -> bool:
        return self._is_visible(self.__arrow_up)

    @allure.step
    def click_arrow_down(self) -> None:
        self._click(self.__arrow_down)