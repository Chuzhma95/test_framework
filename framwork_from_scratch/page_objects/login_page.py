from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from framwork_from_scratch.page_objects.dashboard import DashboardPage
from framwork_from_scratch.utilities.web_ui.base_page import BasePage
import allure


class LoginPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    __email_input = (By.XPATH, '//input[@id="Email"]')
    __password_input = (By.CSS_SELECTOR, '#Password')
    __login_button = (By.XPATH, '//*[@type="submit"]')
    __wrong_message = (By.XPATH, '//*[@class="message-error validation-summary-errors"]')
    __email_error = (By.XPATH, '//*[@id="Email-error"]')

    @allure.step
    def set_email(self, email) -> 'LoginPage':
        self._send_keys(self.__email_input, email)
        return self

    @allure.step
    def set_password(self, password) -> 'LoginPage':
        self._send_keys(self.__password_input, password)
        return self

    @allure.step
    def click_login(self) -> None:
        self._click(self.__login_button)

    @allure.step
    def login(self, email, password):
        self.set_email(email).set_password(password).click_login()
        return DashboardPage(self._driver)

    @allure.step
    def wrong_message_is_visible(self) -> bool:
        return self._is_visible(self.__wrong_message)

    @allure.step
    def email_error_is_visible(self) -> bool:
        return self._is_visible(self.__email_error)
