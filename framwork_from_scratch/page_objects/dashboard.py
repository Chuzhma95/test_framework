from framwork_from_scratch.page_objects.order import OrderPage
from framwork_from_scratch.page_objects.products import ProductPage
from framwork_from_scratch.page_objects.system import SystemInfoPage
from framwork_from_scratch.utilities.web_ui.base_page import BasePage
from selenium.webdriver.common.by import By
import allure


class DashboardPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    __logout_button = (By.XPATH, "//a[@href ='/logout']")
    # for product page
    __catalog_button = (By.XPATH, '//a[@href="#" and @class="nav-link"]//i[@class="nav-icon fas fa-book"]')
    __product_button = (By.XPATH, '//p[text()=" Products"]')
    # for sale page
    __sale_button = (By.XPATH, '//a//i[@class="nav-icon fas fa-shopping-cart"]')
    __orders_button = (By.XPATH, "//p[text() = ' Orders']")
    __vew_ord_button = (By.XPATH, '//*[@class="btn btn-xs btn-info btn-flat margin-l-10"]')
    # for system page
    __system_button = (By.XPATH, '//*[@class="nav-icon fas fa-cube"]')
    __system_inform_button = (By.XPATH, "//p[text() = ' System information']")

    __statistic_plus = (By.XPATH, '//*[@id="nopcommerce-common-statistics-card"]//i[@class="fas fa-plus" or @class="fas fa-minus"]')
    __small_box_info = (By.XPATH, '//div[@class="small-box bg-info"]')
    __small_box_pending = (By.XPATH, '//div[@class="small-box bg-yellow"]')
    __small_box_registered = (By.XPATH, '//div[@class="small-box bg-green"]')
    __small_box_low_stock = (By.XPATH, '//div[@class="small-box bg-red"]')

    @allure.step
    def is_logout_visible(self) -> bool:
        """
        there is a check that the page "Dashboard" has been opened
        :return: button "Logout"
        """
        return self._is_visible(self.__logout_button)

    # catalog
    @allure.step
    def click_catalog(self) -> None:
        self._click(self.__catalog_button)

    @allure.step
    def is_product_visible(self) -> bool:
        """
        check that in list catalog is product
        :return: text "Products"
        """
        return self._is_visible(self.__product_button)

    @allure.step
    def click_product(self):
        self._click(self.__product_button)
        return ProductPage(self._driver)

    # def product(self): # deprecated
    #     return ProductPage(self._driver)

    # sale
    @allure.step
    def click_sale(self) -> None:
        self._click(self.__sale_button)

    @allure.step
    def is_orders_visible(self) -> bool:
        """
        check that in list sale is order
        :return: text "Orders"
        """
        return self._is_visible(self.__orders_button)

    @allure.step
    def click_orders(self):
        self._click(self.__orders_button)
        return OrderPage(self._driver)

    # def sale(self): # deprecated
    #     return SalePage(self._driver)

    # system
    @allure.step
    def click_system(self) -> None:
        self._click(self.__system_button)

    # methods for system_information page
    @allure.step
    def is_system_information_visible(self) -> bool:
        """
        check that in list system is system information
        :return: text "System information"
        """
        return self._is_visible(self.__system_inform_button)

    @allure.step
    def click_system_information(self):
        self._click(self.__system_inform_button)
        return SystemInfoPage(self._driver)

    # methods for statistic
    @allure.step
    def click_static_plus(self) -> None:
        self._click(self.__statistic_plus)

    @allure.step
    def is_small_box_stat_info(self) -> bool:
        """
        visible small box orders on the page "Dashboard"
        :return: text "Orders"
        """
        return self._is_visible(self.__small_box_info)

    @allure.step
    def is_small_box_stat_pending(self) -> bool:
        """
        visible small box pending on the page "Dashboard"
        :return: text "Pending return requests"
        """
        return self._is_visible(self.__small_box_pending)

    @allure.step
    def is_small_box_stat_registered(self) -> bool:
        """
        visible small box registered on the page "Dashboard"
        :return: text "Registered customers"
        """
        return self._is_visible(self.__small_box_registered)

    @allure.step
    def is_small_box_stat_products(self) -> bool:
        """
        visible small box products on the page "Dashboard"
        :return: text "Low stock products"
        """
        return self._is_visible(self.__small_box_low_stock)

    @allure.step
    def vew_button_order(self):
        """
        there is a check that the page "Orders" has been opened after click button
        :return: Order page
        """
        self._click(self.__vew_ord_button)
        return OrderPage(self._driver)
