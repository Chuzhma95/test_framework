from framwork_from_scratch.utilities.web_ui.base_page import BasePage
from selenium.webdriver.common.by import By
import allure


class SystemInfoPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    __nopCommerce_text = (By.XPATH, '//*[@class="col-form-label" and @for="NopVersion"]')
    __nopCommerce_version_field = (By.XPATH, '//*[@for="NopVersion" and text() ="nopCommerce version"]')
    __nopCommerce_version_value = (By.XPATH, '//div[@class="form-text-row" and text()="4.60.0"]')
    __operation_system_field = (By.XPATH, '//*[@for="OperatingSystem"]')
    __operation_system_value = (By.XPATH, '//*[@class="form-text-row" and text()="Microsoft Windows NT 10.0.17763.0"]')
    __asp_field = (By.XPATH, '//*[@for="AspNetInfo"]')
    __asp_value = (By.XPATH, '//*[@class="form-text-row" and text()=".NET 7.0.0"]')
    __trust_level_field = (By.XPATH, '//*[@for="IsFullTrust" and text() = "Is full trust level"]')
    __trust_level_value = (By.XPATH, '//div[@class="form-text-row" and text() = "Yes"]')
    __time_zone_field = (By.XPATH, '//label[@for="ServerTimeZone" and text() = "Server time zone"]')
    __time_zone_value = (By.XPATH, '//div[@class="form-text-row" and text() = "Pacific Standard Time"]')

    @allure.step
    def is_nopCommerce_visible(self) -> bool:
        """
        check that System page is opened
        :return: field nopCommerce
        """
        return self._is_visible(self.__nopCommerce_text)

    @allure.step
    def visible_nopCommerce_version_field(self) -> bool:
        """
        check the display of the field "nopCommerce" on the page
        :return: field nopCommerce
        """
        return self._is_visible(self.__nopCommerce_version_field)

    @allure.step
    def visible_nopCommerce_version_value(self) -> bool:
        """
        check the display of the value on the field "nopCommerce"
        :return: value "4.50.0"
        """
        return self._is_visible(self.__nopCommerce_version_value)

    @allure.step
    def visible_operation_system_field(self) -> bool:
        """
        check the display of the field "operation_system" on the page
        :return: field "operation_system"
        """
        return self._is_visible(self.__operation_system_field)

    @allure.step
    def visible_operation_system_value(self) -> bool:
        """
         check the display of the value on the field "operation_system"
        :return: field "Microsoft Windows NT 10.0.17763.0"
        """
        return self._is_visible(self.__operation_system_value)

    @allure.step
    def visible_asp_field(self) -> bool:
        """
        check the display of the field "ASP.NET info" on the page
        :return: field "ASP.NET info"
        """
        return self._is_visible(self.__asp_field)

    @allure.step
    def visible_asp_value(self) -> bool:
        """
        check the display of the value on the field "ASP.NET info"
        :return: field ".NET 6.0.1"
        """
        return self._is_visible(self.__asp_value)

    @allure.step
    def visible_trust_level_field(self) -> bool:
        """
        check the display of the field on the field "Is full trust level"
        :return: field "Is full trust level"
        """
        return self._is_visible(self.__trust_level_field)

    @allure.step
    def visible_trust_level_value(self) -> bool:
        """
        check the display of the value on the field "Is full trust level"
        :return: field "Yes"
        """
        return self._is_visible(self.__trust_level_value)

    @allure.step
    def visible_time_zone_field(self) -> bool:
        """
        check the display of the field on the field "time zone"
        :return: field "time zone"
        """
        return self._is_visible(self.__time_zone_field)

    @allure.step
    def visible_time_zone_value(self) -> bool:
        """
        check the display of the value on the field "time zone"
        :return: "Pacific Standard Time"
        """
        return self._is_visible(self.__time_zone_value)