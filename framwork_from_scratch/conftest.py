from contextlib import suppress
import json
import pathlib
import pytest

from framwork_from_scratch.data_api_classes.zippo import Zippo
from framwork_from_scratch.page_objects.login_page import LoginPage
from framwork_from_scratch.utilities.configuration import Configuration
from framwork_from_scratch.utilities.driver_factory import DriverFactory
from framwork_from_scratch.utilities.read_configs import ReadConfig
from framwork_from_scratch.utilities.configuration import config
import allure


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep


@pytest.fixture()
def create_driver(request):
    chrome_driver = DriverFactory.create_driver(config.browser_id)
    chrome_driver.maximize_window()
    chrome_driver.get(config.base_url)
    yield chrome_driver
    if request.node.rep_call.failed:
        with suppress(Exception):
            allure.attach(chrome_driver.get_screenshot_as_png(), name=request.function.__name__, attachment_type=allure.attachment_type.PNG)
    chrome_driver.quit()


@pytest.fixture()
def open_login_page(create_driver):
    return LoginPage(create_driver)


@pytest.fixture()
def dashboard_page(open_login_page):
    login_page = open_login_page
    dashboard_page = login_page.login(config.user_name, config.user_password)
    return dashboard_page


@pytest.fixture()
def open_product_page(dashboard_page):
    dashboard_page.click_catalog()
    dashboard_page.is_product_visible()
    product_page = dashboard_page.click_product()
    return product_page


@pytest.fixture()
def open_order_page(dashboard_page):
    dashboard_page.click_sale()
    dashboard_page.is_orders_visible()
    order_page = dashboard_page.click_orders()
    return order_page


@pytest.fixture()
def open_system_information_page(dashboard_page):
    dashboard_page.click_system()
    dashboard_page.is_system_information_visible()
    system_information = dashboard_page.click_system_information()
    return system_information


# for api

@pytest.fixture()
def create_zippo():
    return Zippo()
