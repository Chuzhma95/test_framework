import json


class Zippo:
    def __init__(self, **kwargs):
        self.country = 'United States' if 'country' not in kwargs.keys() else kwargs['country']
        self.country_abbreviation = 'US' if 'country abbreviation' not in kwargs.keys() else kwargs[
            'country abbreviation']
        self.places = [{"place name": "Miami", "longitude": "-80.183", "state": "Florida", "state abbreviation": "FL",
                        "latitude": "25.9286"}] if 'places' not in kwargs.keys() else kwargs['places']

    @classmethod
    def create_from_json(cls, **kwargs):
        return cls(**kwargs)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def update_dict(self, **kwargs):
        self.__dict__.update(kwargs)

    def get_json(self):
        return json.dumps(self.__dict__)

    def get_dict(self):
        return self.__dict__
