from framwork_from_scratch.data_api_classes.zippo import Zippo
from framwork_from_scratch.utilities.api.BaseAPI import BaseAPI
import allure


class ZippoAPI(BaseAPI):
    def __init__(self):
        super().__init__()
        self.__url = "/us"

    @allure.step
    def get_zippo_by_id(self, zippo_id, headers=None):
        return self.get(f'{self.__url}/{zippo_id}', headers=headers)

    @allure.step
    def create_zippo(self, body=None, zip_id=None):
        zippo_data = Zippo()
        if body is not None:
            zippo_data.update_dict(**body)
        response = self.post(f'{self.__url}/{zip_id}', body=zippo_data.get_json())
        return response

