from framwork_from_scratch.utilities.data_base.base_repository import BaseRepo
from db_repositories.orders_repository import OrdersRepository


class ProductRepository(BaseRepo):
    def __init__(self):
        super().__init__()
        self.table_name = 'products'
        self.table_orders = OrdersRepository().table_name

    def get_product_by_id(self, product_id):
        self._cursor.execute(f"select * from {self.table_name} where products.id = {product_id}")
        return self._cursor.fetchone()

    def insert_one(self, name, price):
        self._cursor.execute(f"insert into {self.table_name} (name, price) values ('{name}', {price});")

    def delete_by_name(self, name):
        self._cursor.execute(f"delete from {self.table_name} where products.name = '{name}';")

    def get_data_products_quantity_total(self):
        self._cursor.execute(f"SELECT {self.table_name}.name, {self.table_name}.price, {self.table_orders}.quantity, {self.table_name}.price * {self.table_orders}.quantity AS total FROM {self.table_name} LEFT JOIN {self.table_orders} ON {self.table_name}.id = {self.table_orders}.product_id;")
        return self._cursor.fetchall()
