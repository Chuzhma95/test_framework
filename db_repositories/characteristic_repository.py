from framwork_from_scratch.utilities.data_base.base_repository import BaseRepo


class CharacteristicTable(BaseRepo):
    def __init__(self):
        super().__init__()
        self.table_name = 'characteristic'

    def create_table(self):
        self._cursor.execute(
            f"create table {self.table_name} (id serial PRIMARY KEY, diagonal FLOAT, amount_sim INT, product_id INT );")

    def alert_characteristic_table(self):
        self._cursor.execute(
            f"ALTER TABLE {self.table_name} ADD CONSTRAINT fk_characteristic_products FOREIGN KEY (product_id) REFERENCES products (id);")

    def insert_plus_one(self, diagonal, amount_sim, product_id):
        self._cursor.execute(f"insert into {self.table_name} (diagonal, amount_sim, product_id) values ({diagonal}, {amount_sim}, {product_id});")
