from framwork_from_scratch.utilities.data_base.base_repository import BaseRepo


class OrdersRepository(BaseRepo):
    def __init__(self):
        super().__init__()
        self.table_name = 'orders'

    def insert_one(self, quantity, product_id):
        self._cursor.execute(f"insert into {self.table_name} (quantity, product_id) values ('{quantity}', {product_id});")
