from mongo_db_collection.vag_collection import VagMongo

test = VagMongo()

#Insert
test.insert_one({'name': 'Skoda', 'model': 'Octavia', 'budget': '30000'})
test.insert_many({'name': 'Bugatti', 'model': 'Veyron', 'budget': '100000'},
                 {'name': 'Volkswagen', 'model': 'Passat', 'budget': '40000'})

#Find
cursor = test.find_name({'name': 'Porshe'})
print(cursor)

#Sort
result = test.find_and_sort({}, 'budget')
print(list(result))

#Delete
delete = test.delete_for_model({'model': 'Golf'})
